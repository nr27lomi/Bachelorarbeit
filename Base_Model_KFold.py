import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import tensorflow as tf
import tensorflow_addons as tfa

from tensorflow import keras
from tensorflow.keras import layers
from tensorflow.keras.callbacks import EarlyStopping
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import StratifiedKFold
from tensorflow.keras.layers.experimental import preprocessing

seed = 202
tf.random.set_seed(seed)

plt.rc('figure', autolayout=True)
plt.rc('axes', labelweight='bold', titleweight='bold', titlesize=20)

bee_csv = pd.read_csv(r"E:\Uni\Informatik\Bachelorarbeit\Data\bee_data.csv")
healthOneHot = pd.get_dummies(bee_csv['health'], drop_first=False, sparse=False)
y_Array = healthOneHot.to_numpy().argmax(axis=1)

bee_data = []
health_amounts = bee_csv['health'].value_counts().plot(kind='bar')
health_amounts.set_title('Value distribution in the Dataset')
plt.show()

for i in range(len(bee_csv['health'])):
    picture = tf.io.read_file(r"E:\Uni\Informatik\Bachelorarbeit\Data\bee_imgs\bee_imgs"+"\\"+bee_csv['file'][i])
    picture = tf.io.decode_png(picture, channels=3)
    picture = tf.image.convert_image_dtype(picture, dtype=tf.float32)
    picture = tf.image.resize(picture, (128, 128))
    #picture = tf.image.rgb_to_grayscale(picture)
    picture = picture.numpy()
    bee_data.append(picture)

bee_pictures = np.stack(bee_data)
Kfold = StratifiedKFold(n_splits=5, random_state=seed, shuffle=True)
lossFolds, accuracyFolds, recallFolds, confmatrixFolds, precisionFolds, F1Folds = [], [], [], [], [], []
foldnumber = 0

for train_index, val_index in Kfold.split(bee_pictures, y_Array):
    X_train, X_val, y_train, y_val = bee_pictures[train_index], bee_pictures[val_index], healthOneHot.loc[train_index,:], healthOneHot.loc[val_index,:]
    model = keras.Sequential([
        preprocessing.RandomContrast(1.0, seed=seed),
        preprocessing.RandomFlip('horizontal', seed=seed),
        #Model Base
        layers.Conv2D(filters=32, kernel_size=3, activation='relu', padding='same', input_shape=[128, 128, 1]),
        layers.MaxPool2D(strides=(2, 2), padding='same'),
        layers.Conv2D(filters=64, kernel_size=3, activation='relu', padding='same'),
        layers.MaxPool2D(strides=(2, 2), padding='same'),
        layers.Conv2D(filters=128, kernel_size=3, activation='relu', padding='same'),
        layers.MaxPool2D(strides=(2, 2), padding='same'),

        #Model Head
        layers.Flatten(),
        layers.Dropout(0.3, seed=seed),
        layers.Dense(units=128, activation='relu'),
        layers.Dense(units=256, activation='relu'),
        layers.Dense(units=6, activation='softmax')
    ])

    earlyStopping = EarlyStopping(
        min_delta=0.002,
        patience=50,
        restore_best_weights=True
    )

    model.compile(
        optimizer=tf.keras.optimizers.Adam(),
        loss='categorical_crossentropy',
        metrics=['accuracy', tf.keras.metrics.Precision(), tf.keras.metrics.Recall()]
    )

    train_hist = model.fit(
        X_train,
        y_train,
        validation_data=(X_val, y_val),
        epochs=500,
        callbacks=[earlyStopping]
    )
    Evaluation = model.evaluate(bee_pictures, healthOneHot, return_dict=True)
    if foldnumber==0:
        lossFolds.append(Evaluation['loss'])
        accuracyFolds.append(Evaluation['accuracy'])
        recallFolds.append(Evaluation['recall'])
        precisionFolds.append(Evaluation['precision'])
    else:
        lossFolds.append(Evaluation['loss'])
        accuracyFolds.append(Evaluation['accuracy'])
        recallFolds.append(Evaluation['recall'+'_'+str(foldnumber)])
        precisionFolds.append(Evaluation['precision'+'_'+str(foldnumber)])

    confusion_predictions = model.predict(bee_pictures)
    confmatrixFolds.append(confusion_matrix(y_Array, confusion_predictions.argmax(axis=1)))
    f1 = tfa.metrics.F1Score(num_classes=6)
    f1.update_state(healthOneHot, confusion_predictions)
    result = f1.result()
    F1Folds.append(result.numpy())
    foldnumber+=1
print("loss:")
print(lossFolds)
print(np.mean(lossFolds))
print("accuracy:")
print(accuracyFolds)
print(np.mean(accuracyFolds))
print("recall:")
print(recallFolds)
print(np.mean(recallFolds))
print("precision:")
print(precisionFolds)
print(np.mean(precisionFolds))
for i in confmatrixFolds:
    print(i)
for i in range(0, 6):
    print("F1 for class ", i, ":")
    print(np.mean([F1Folds[0][i], F1Folds[1][i], F1Folds[2][i], F1Folds[3][i], F1Folds[4][i]]))
