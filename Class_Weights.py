import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import tensorflow as tf
import tensorflow_addons as tfa

from tensorflow import keras
from tensorflow.keras import layers
from tensorflow.keras.callbacks import EarlyStopping
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix
from tensorflow.keras.layers.experimental import preprocessing
from sklearn.utils import class_weight

seed = 202
tf.random.set_seed(seed)

plt.rc('figure', autolayout=True)
plt.rc('axes', labelweight='bold', titleweight='bold', titlesize=20)

bee_csv = pd.read_csv(r"E:\Uni\Informatik\Bachelorarbeit\Data\bee_data.csv")
healthOneHot = pd.get_dummies(bee_csv['health'], drop_first=False)
y_Array = healthOneHot.to_numpy().argmax(axis=1)
class_weights = class_weight.compute_class_weight('balanced',
                                                  classes=np.unique(bee_csv['health']),
                                                  y=bee_csv['health'])
class_weights = dict(enumerate(class_weights))
print(class_weights)
bee_data = []
health_amounts = bee_csv['health'].value_counts().plot(kind='bar')
health_amounts.set_title('Value distribution in the Dataset')
plt.show()

for i in range(len(bee_csv['health'])):
    picture = tf.io.read_file(r"E:\Uni\Informatik\Bachelorarbeit\Data\bee_imgs\bee_imgs"+"\\"+bee_csv['file'][i])
    picture = tf.io.decode_png(picture, channels=3)
    picture = tf.image.convert_image_dtype(picture, dtype=tf.float32)
    picture = tf.image.resize(picture, (128, 128))
    picture = tf.image.rgb_to_grayscale(picture)
    picture = picture.numpy()
    bee_data.append(picture)

bee_pictures = np.stack(bee_data)

X_train, X_hold, y_train, y_hold = train_test_split(bee_pictures, healthOneHot, test_size=0.3, random_state=seed, stratify=healthOneHot)
X_val, X_test, y_val, y_test = train_test_split(X_hold, y_hold, test_size=1/3, random_state=seed, stratify=y_hold)

model = keras.Sequential([
    preprocessing.RandomContrast(1.0, seed=seed),
    preprocessing.RandomFlip('horizontal', seed=seed),
    #Model Base
    layers.Conv2D(filters=32, kernel_size=3, activation='relu', padding='same', input_shape=[128, 128, 1]),
    layers.MaxPool2D(strides=(2, 2), padding='same'),
    layers.Conv2D(filters=64, kernel_size=3, activation='relu', padding='same'),
    layers.MaxPool2D(strides=(2, 2), padding='same'),
    layers.Conv2D(filters=128, kernel_size=3, activation='relu', padding='same'),
    layers.MaxPool2D(strides=(2, 2), padding='same'),

    #Model Head
    layers.Flatten(),
    layers.Dropout(0.3, seed=seed),
    layers.Dense(units=128, activation='relu'),
    layers.Dense(units=256, activation='relu'),
    layers.Dense(units=6, activation='softmax')
])

earlyStopping = EarlyStopping(
    min_delta=0.002,
    patience=50,
    restore_best_weights=True
)

model.compile(
    optimizer=tf.keras.optimizers.Adam(),
    loss='categorical_crossentropy',
    metrics=['accuracy', tf.keras.metrics.Precision(), tf.keras.metrics.Recall()]
)

train_hist = model.fit(
    X_train,
    y_train,
    validation_data=(X_val, y_val),
    epochs=500,
    callbacks=[earlyStopping],
    class_weight=class_weights
)
prediction = model.evaluate(X_test, y_test)
accuracyplot = pd.DataFrame(train_hist.history).loc[: ,['accuracy', 'val_accuracy']].plot()
recallplot = pd.DataFrame(train_hist.history).loc[: ,['recall', 'val_recall', 'precision', 'val_precision']].plot()
plt.show()
model.evaluate(bee_pictures, healthOneHot)
confusion_predictions = model.predict(bee_pictures)
print(confusion_matrix(y_Array, confusion_predictions.argmax(axis=1)))
f1 = tfa.metrics.F1Score(num_classes=6)
f1.update_state(healthOneHot, confusion_predictions)
result = f1.result()
print(result.numpy())
